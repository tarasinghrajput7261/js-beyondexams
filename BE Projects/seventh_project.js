
// a function to get the input as an arrgument in arr parameter
function countingSocksPair(arr) {
// creating an empty object to store the colour name as a key and how many time its apprearing in the parameter as a value
    let socks = {};
    // for loop that stores how much time a specific colour has been appreared
    for (let i = 0; i < arr.length; i++) {
        // a ternary operator to check if the same colour has appreared more than 1 time then its value will increment by 1
        socks[arr[i]] ? socks[arr[i]]++ : socks[arr[i]] = 1;
    }
// empty object to store the colour name as key and how many pair it contains as its value 
    let socks_pairs = {};
    // its a variable to count all the no. of pairs the argument contains
    let total_pairs = 0;
    // for in loop which loops thorugh all the keys of the provided object
    for (let key in socks) {
        // 
        total_pairs += Math.floor(socks[key] / 2);

        socks_pairs[key] = Math.floor(socks[key] / 2);
    }

    let result = '';
    for (let i in socks_pairs) {
        if (socks_pairs[i] !== 0)
        result += `${socks_pairs[i]} ${i} pair`;
    }
    result = result.split(/(?<=pair)/).join(' + ');

    return `${total_pairs} (${result})`;
}

console.log(countingSocksPair(["red", "blue", "red", "black", "red", "blue", "green", "black", "black"]));