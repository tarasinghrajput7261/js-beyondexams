// this line initialises a variable and assigns a value to it in this case its 69 ;)
let minutes = 69;
// this line uses the basic arithmatic operator multiplication operator to convert the minutes value to seconds 😁
let seconds = minutes * 60;
// this line prints the value of the seconds after the calculation is done 👌
console.log(seconds);