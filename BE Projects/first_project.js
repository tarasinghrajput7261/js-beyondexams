

// Creating a variable and assigning a number
let input = 69;
// printing the type of the value which is assigned in input variable
console.log(typeof input);
// Reassigning a variable with a name
input = 'Tara Singh Rajput';
// printing the type of the value which is assigned in input variable
console.log(typeof input);
// Reassigning a variable with a boolean value
input = false;
// printing the type of the value which is assigned in input variable
console.log(typeof input);
