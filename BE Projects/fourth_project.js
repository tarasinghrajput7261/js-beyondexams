// these lines initialises a variable and assigns the value ;)

let num1 = 96;
let num3 = 69;


// this if else statements checks for if the num1 variable has a even or odd number

if (num1%2 === 0) {
    console.log(`${num1} is even`);
} else {
    console.log(`${num1} is odd`);
}

// this switch case statements checks for if the num1 variable has a even or odd number
switch (num3%2 == 0) {
    case true:
        console.log(`${num3} is even`);
        break;

    default:
        console.log(`${num3} is odd`);
        break;
}

