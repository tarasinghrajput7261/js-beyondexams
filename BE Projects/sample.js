// the arrow function which has a input as a parameter and a ternary operator which whether the input string's length is 0 or not
// return true if 0 and 1 if  false
const is_blank = (input) => {return (input.length === 0 ? true : false);}
console.log(is_blank(''));
console.log(is_blank('abc'));



// Here the Arrow function named length() is used and "input" as a parameter, 
// then the input's value is first converted to string and then returns the length  of the string

// const length = (input) => {return (input.toString().length)}
function length(input) {
    return (input.toString().length)
}


// here the value 69 first is converted to string and then the length of it is printed
console.log(length(69));
// here since the value "Tara" and "" is already a string its length is printed
console.log(length("Tara"));
console.log(length(''));
console.log(length('Siddhesh'));
console.log(length("Siddhesh's Mobile"));
