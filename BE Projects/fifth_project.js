
// here i made an object named mySelf and with rspective key and values 😁
let mySelf = {
    name : 'Tara Singh Rajput',
    age : 18,
    dob : '29/06/2004',
    motherName : 'Bhuri Bai',
    fatherName : 'Bhim Singh Rajput'
}
// For in loop is used to temporarily store keys of myself object in key named variable 😜
for (let key in mySelf) {
    console.log(`my ${key} is ${mySelf[key]}`);
}